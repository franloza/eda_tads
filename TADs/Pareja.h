#pragma once

template<class A, class B>
class Pareja {
public:
	Pareja() {};
	Pareja(A prim, B seg) : _prim(prim), _seg(seg) {};
	A primero() const { return _prim; };
	B segundo() const { return _seg; };

	unsigned int hash() const {
		return ::hash(_prim) * 1021 + ::hash(_seg);
	};

	bool operator==(const Pareja& otra) const {
		return _prim == otra._prim && _seg == otra._seg;
	}
private:
	A _prim;
	B _seg;
};